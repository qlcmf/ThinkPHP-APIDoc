<?php

use think\facade\Route;

// DOC
Route::group('_doc', function () {
	Route::get('list', "Hua\ApiDoc\DocController@getDocList");
	Route::get('info', "Hua\ApiDoc\DocController@getDocInfo");
	Route::any('debug', "Hua\ApiDoc\DocController@debugDoc");
	Route::any('pass', "Hua\ApiDoc\DocController@pass");
	Route::any('consoleDoc', "Hua\ApiDoc\DocController@consoleDoc");
	Route::any('login', "Hua\ApiDoc\DocController@login");
	Route::any('index', "Hua\ApiDoc\DocController@index");
	Route::any('signin', "Hua\ApiDoc\DocController@signin");
	//
	Route::get('socketclient', "Hua\ApiDoc\DocController@socketclient");
});