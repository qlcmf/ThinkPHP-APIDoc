<?php

namespace Hua\ApiDoc;

use think\facade\Cookie;
use think\facade\View;

/**
 * Class DocController
 * @package Hua\ApiDoc
 */
class DocController extends ApiDocBase {

	/**
	 * @title  index 文档首页
	 * @return string
	 * @throws \Exception
	 */
	public function index() {
		if ($this->chkLogin()) {
			return redirect((string)'/_doc/login');
		}
		$data['_leftDocList'] = $this->doc->getList();
		return View::fetch('/index', $data);
	}

	/**
	 * @title login 登陆
	 */
	public function login() {
		if ($this->request->isPost()) {
			$password = $this->request->post('password');
			if ($password == $this->doc->__get("password")) {
				cookie('_login_doc', true);
				return response([
					'code' => 200,
					'msg'  => '登陆成功！',
				], 200, [], 'json');
			}
			return response([
				'code' => 500,
				'msg'  => '密码错误！',
			], 200, [], 'json');
		}
		$data = [];
		return View::fetch('/login', $data);
	}

	/**
	 * @title  chkLogin
	 * @return bool|\think\response\Redirect
	 */
	private function chkLogin() {
		if (empty($this->doc->__get("password")) || cookie('_login_doc')) {
			return false;
		}
		return true;
	}

	/**
	 * @title  consoleDoc
	 * @return string
	 * @throws \Exception
	 */
	public function consoleDoc() {
		$data = [];
		return View::fetch('/console', $data);
	}

	/**
	 * 接口列表 可以用户接口搜索等操作
	 * @return \think\Response
	 */
	/*public function getDocList() {
		$list = $this->doc->getList();
		return response(['firstId' => '', 'list' => $list], 200, [], 'json');
	}*/

	/**
	 * 接口详情
	 *
	 * @param string $name
	 *
	 * @return mixed
	 * @throws \Exception
	 */
	public function getDocInfo() {
		$name = $this->request->get('name');
		[$class, $action] = explode("::", $name);
		$actionDoc = $this->doc->getInfo($class, $action);

		if (!$actionDoc) {
			return $this->result(null, 200, '参数错误！');
		}
		$return              = $this->doc->formatReturn($actionDoc);
		$actionDoc['header'] = isset($actionDoc['header']) ? array_merge($this->doc->__get('public_header'), $actionDoc['header']) : [];
		if (cookie('_userTokenType') == 'bearer_token') {
			$actionDoc['header'] = array_merge(
				[
					[
						'name'    => 'Authorization',
						'require' => 0,
						'default' => '',
						'desc'    => 'Bearer token 认证',
					]
				]
				, $actionDoc['header']);
		}

		$actionDoc['param'] = isset($actionDoc['param']) ? array_merge($this->doc->__get('public_param'), $actionDoc['param']) : [];


		/*var_dump($actionDoc);
		exit();*/
		return View::fetch('/info', ['doc' => $actionDoc, 'return' => $return]);
	}

	/**
	 * @title  debugDoc 接口访问测试
	 * @return \think\Response
	 */
	public function debugDoc() {
		$data          = $this->request->param();
		$apiUrl        = $this->request->param('url');
		$method        = $this->request->param('method_type', 'GET');
		$cookie        = $this->request->param('cookie');
		$headers       = $this->request->param('header/a', []);
		$res['code']   = '404';
		$res['msg']    = '接口地址无法访问';
		$res['result'] = '';
		unset(
			$data['method_type'],
			$data['url'],
			$data['url'],
			$data['cookie'],
			$data['header'],
			$data['addon'],
			$data['controller'],
			$data['action']
		);

		$result               = $this->httpRequest($apiUrl, $cookie, $data, $method, $headers);
		$res['result']        = $result['output'];
		$res['response_head'] = $result['response_head'];
		if ($res['result']) {
			$res['code'] = 200;
			$res['msg']  = 'success';
		}
		return response($res, 200, [], 'json');
	}

	/**
	 * @title  signin
	 * @return \think\Response
	 */
	public function signin() {
		$type          = $this->request->param('type');
		$authorization = $this->request->param('Authorization', '');
		$res           = [];
		if ($type) {
			if ($type == '_clear') {
				Cookie::delete('_userToken');
				$res['result']  = cookie('_userToken');
				$res['code']    = '200';
				$res['meaasge'] = '清楚成功！';
				return response($res, 200, [], 'json');
			} elseif ($type == 'bearer_token') {
				cookie('_userTokenType', $type, 3600 * 24);
				cookie('_userToken', 'Bearer ' . $authorization, 3600 * 24);
				$res['result'] = $type;
			}
			$res['result'] = 'ERROR';
		}
		$userInfo = [];
		// 清楚登录状态
		if ($res['result']) {
			$res['code']    = '200';
			$res['meaasge'] = $userInfo;
		}
		return response($res, 200, [], 'json');
	}

	/**
	 * @title  socketclient
	 * @return string
	 */
	public function socketclient() {
		$data = [];
		return View::fetch('/socket_client', $data);
	}
}