<?php
declare (strict_types=1);

namespace Hua\ApiDoc;

use Hua\ApiDoc\lib\Doc;
use think\App;
use think\exception\ValidateException;
use think\facade\Config;
use think\facade\View;
use think\Validate;

/**
 * 控制器基础类
 */
abstract class ApiDocBase {

	/**
	 * Request实例
	 * @var \think\Request
	 */
	protected $request;

	/**
	 * 应用实例
	 * @var \think\App
	 */
	protected $app;

	/**
	 * @var Doc
	 */
	protected $doc;

	/**
	 * 控制器中间件
	 * @var array
	 */
	protected $middleware = [];

	/**
	 * 构造方法
	 * @access public
	 *
	 * @param App $app 应用对象
	 */
	public function __construct(App $app) {
		$this->app     = $app;
		$this->request = $this->app->request;

		$this->_init();
		// 控制器初始化
		$this->initialize();
	}

	// 初始化
	protected function initialize() {
	}

	protected function _init() {
		$this->doc = new Doc((array)Config::get('doc'));
		View::config([
			'view_path' => __DIR__ . DIRECTORY_SEPARATOR . 'view' . DIRECTORY_SEPARATOR
		]);

		$group = request()->get('module') ?? 'api';
		cookie('_module', $group);
		View::assign([
			'module'      => request()->get('module', 'api'),
			'domain'      => $this->request->domain(),
			'root'        => $this->request->root(),
			'title'       => $this->doc->__get("title"),
			'module_list' => $this->doc->__get("module_list"),
			'version'     => $this->doc->__get("version"),
			'copyright'   => $this->doc->__get("copyright"),
			'_userToken'  => cookie('_userToken'),
		]);
	}

	/**
	 * curl模拟请求方法
	 *
	 * @param       $url
	 * @param       $cookie
	 * @param array $data
	 * @param       $method
	 * @param array $headers
	 *
	 * @return mixed
	 */
	protected function httpRequest($url, $cookie, $data = [], $method = [], $headers = []) {
		$curl = curl_init();
		if (count($data) && $method == "GET") {
			$data = array_filter($data);
			$url  .= "?" . http_build_query($data);
			$url  = str_replace(['%5B0%5D'], ['[]'], $url);
		}
		curl_setopt($curl, CURLOPT_URL, $url);
		/** @noinspection CurlSslServerSpoofingInspection */
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
		/** @noinspection CurlSslServerSpoofingInspection */
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
		$head = [];
		foreach ($headers as $name => $value) {
			$head[] = $name . ":" . $value;
		}

		$head = array_merge([
			'Accept: application/json',
		], $head);
		// 认证方式
		if (cookie('_userTokenType') == 'bearer_token') {
			$head = array_merge(['Authorization: ' . cookie('_userToken')], $head);
		}

		curl_setopt($curl, CURLOPT_HTTPHEADER, $head);
		$method = strtoupper($method);
		switch ($method) {
			case 'GET':
				break;
			case 'POST':
				curl_setopt($curl, CURLOPT_POST, true);
				curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
				break;
			case 'PUT':
				curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PUT');
				curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
				break;
			case 'DELETE':
				curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'DELETE');
				break;
		}
		if (!empty($cookie)) {
			curl_setopt($curl, CURLOPT_COOKIE, $cookie);
		}
		curl_setopt($curl, CURLINFO_HEADER_OUT, TRUE);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		$result['output']        = curl_exec($curl);
		$result['response_head'] = curl_getinfo($curl);
		curl_close($curl);
		return $result;
	}
}